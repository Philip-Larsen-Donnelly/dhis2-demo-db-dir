# dhis2-demo-db-dir

### DHIS 2 demo databases (in **directory** format)

This repository contains PostgreSQL dumps of demo databases.

To work with the database:

```
# go into the directory <db-directory> representing the DB you want to import (e.g. `<path to repo>/sierra-leone/2.31/dhis2-db-sierra-leone`)
cd <db-directory>

# remap the files to the names used by pg_restore
source unmap

# restore into the database <db-name>
sudo -u postgres pg_restore -d <db-name> -j 8 <db-directory>
```


After modifying it you can create a dump using pg_dump. Exclude the analytics and resource tables as they will be generated later and take up a lot of space:

```
# dump the database <db-name> to the target <db-directory>
sudo -u postgres pg_dump --compress=0 --format=directory --file=<db-directory> --jobs=10 --dbname=<db-name> -T analytics* -T _*

# create the mapping between the dumped output and the database tables
sudo -u postgres pg_restore -l -F d <db-directory> | grep "TABLE DATA" | sed 's/\(.*\);.* TABLE DATA [^ ]* \([^ ]*\) .*/mv \1.dat \2.dat/' > <db-directory>/map
sudo -u postgres pg_restore -l -F d <db-directory> | grep "TABLE DATA" | sed 's/\(.*\);.* TABLE DATA [^ ]* \([^ ]*\) .*/mv \2.dat \1.dat/' > <db-directory>/unmap

# rename the files to match the table names
cd <db-directory>
source map
```


Follow the naming convention for directories under the sierra-leone directory, which is the DHIS 2 version number (e.g. "2.28") and "dev" (snapshot).

Remember to write good commit messages to explain what you do with the databases.
